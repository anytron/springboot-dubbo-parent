
### springboot-dubbo


```
更新maven配置版本以及代码注解
dobbo 更新至 3.1.0 版本
springboot 更新至 2.3.12.RELEASE 版本
```

```
apollo配置如无需,请关闭提供者配置项,默认是开启的,消费者也可以按提供者配置来配置apollo
apollo主要目的用于系统配置中心,和dubbo配置中心

关闭apollo:
    提供者spring-boot-dubbo-service
    注释掉启动类中的注解@EnableApolloConfig
    注释掉application.yml的apollo配置
app: 
  id: demo
apollo: 
  cluster: default
  meta: http://192.168.48.100:8080
  cache-dir: ./spring-boot/logs/
  bootstrap: 
    enabled: true
  autoUpdateInjectedSpringProperties: true
    
    注释掉dubbo的配置中心:
dubbo: 
  config-center:
    address: apollo://192.168.48.100:8080 #apollo地址 其他配置自动使用环境默认apollo配置
    namespace: ${spring.application.name} #dubbo的apollo命名空间自定义,一般就用项目名称即可
    config-file: application.yml #映射dubbo配置所在的配置文件,根据自己情况填写
    group: ${spring.application.name}
```

### SpringBoot-dubbo集成基础框架结构
 集成了
- `springboot`
- `dubbo`
- `pagehelper`
- `tkmapper`
- `mybatis`
- `mysql or postgresql`
- `zookeeper`  
- `apollo`  
- `prometheus`  
是一个母子项目可接入多个子项目进行扩展方式开发  

项目默认使用了直连方式方便测试开发使用,发布需要使用`zookeeper`作为服务中间件  

- spring-boot-dubbo-service   
    是服务提供者,用来提供集中性服务

- spring-boot-dubbo-public   
    作为中间包组件用于提供者与消费者之间的共用,比如接口,model,po,vo,utils等  

- spring-boot-dubbo-client   
    是服务消费者,用于消费服务一般同时可以作为与前端交互的服务提供者,一般不进行任何数据库操作由提供者操作  

