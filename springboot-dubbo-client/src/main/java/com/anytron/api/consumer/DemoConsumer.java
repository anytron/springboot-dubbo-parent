package com.anytron.api.consumer;

import java.util.Map;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.anytron.api.ISpringBootDemoProvider;
import com.anytron.model.vo.Demo;
import com.anytron.model.vo.PageParam;

@RestController
@RequestMapping("/demo")
public class DemoConsumer {
	
	@DubboReference(version="1.0.0")
	ISpringBootDemoProvider demoProvider;
	
	@RequestMapping(value="/test")
	@ResponseBody
	public Demo getDemo(String name) {
		return demoProvider.getDemo(name);
	}
	
	@RequestMapping(value="/getapps")
	@ResponseBody
	public Map<String, Object> getDemo(PageParam pp) {
		return demoProvider.getApps(pp);
	}

}
