package com.anytron;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;

import tk.mybatis.spring.annotation.MapperScan;

@MapperScan(basePackages = "com.anytron.mapper")
@EnableTransactionManagement
@SpringBootApplication
@EnableApolloConfig
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
}
