package com.anytron.api.provider;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.anytron.api.ISpringBootDemoProvider;
import com.anytron.model.vo.Demo;
import com.anytron.model.vo.PageParam;
import com.anytron.service.IAppService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@DubboService(version="1.0.0")
public class DemoProvider implements ISpringBootDemoProvider {
	
	@Autowired
	IAppService appService;
	
	@Value("${dubbo.protocol.port}")
	String port;
	
	@Override
	public Demo getDemo(String name) {
		log.info("进来了.............port:"+port);
		Demo demo = new Demo();
		if (StringUtils.isBlank(name)) {
			demo.setAge(10);
			demo.setName(port);
		} else if ("aaa".equals(name)) {
			demo.setAge(20);
			demo.setName("bbb");
		} else {
			demo.setAge(30);
			demo.setName("ccc");
		}
		return demo;
	}

	@Override
	public Map<String, Object> getApps(PageParam pp) {System.out.println("afffffffffff");
		return appService.getApps(pp);
	}

}
